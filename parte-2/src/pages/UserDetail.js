import { useState, useEffect } from 'react'
import { useParams } from "react-router-dom"
import { useSelector, useDispatch } from 'react-redux'
import { selectedUser, deleteUser } from '../services/actions/usersActions'
import UpdateForm from '../components/Users/UpdateForm'
import axios from 'axios'
import '../css/Users/UserDetail.css'

const UserDetail = (props) => {
    const {ID} = useParams();
    const dispatch = useDispatch();
    const [address, setAddress] = useState([]);
    const [company, setCompany] = useState([])
    const [updateButton, setUpdateButtton] = useState(false);
    const userState = useSelector((state) => state.user);
    
    const fetchUserDetail = async () => {
        const res = await axios.get(`https://jsonplaceholder.typicode.com/users/${ID}`).catch(err => {
            console.log(err)
        });

        dispatch(selectedUser(res.data));
        setAddress(res.data.address);
        setCompany(res.data.company);

    }

    useEffect(() => {
        if(ID && ID !== ""){
            fetchUserDetail();
        }
        
    }, [ID]);

    const Update = ()=>{
        if (updateButton){
            setUpdateButtton(false);
        }else {
            setUpdateButtton(true);
        }
    }

    const Delete = async () =>{
        const res = await axios.delete(`https://jsonplaceholder.typicode.com/users/${ID}`)
        .catch(err => {
            console.log(err);
        })

        if(res.status === 200){
            console.log(res.status);
            alert('Usuario eliminado con exito', res.status);


            dispatch(deleteUser(ID));

        }
    }

    const ActualizarForm = async (userForm) => {
        //console.log(userForm);
        const res = await axios.put(`https://jsonplaceholder.typicode.com/users/${ID}`, userForm).catch(err => {
            console.log(err);
        });

        if(res.status === 200){
            console.log(res);
            alert('Usuario Actualizado con exito', res.status);

        }


    }

    return (
        <div className="bloque-userDetail">
            
            <div>
                <h1>User { userState.id } Info</h1>
            </div>
            <div className="caja-detail">
                <div>
                    <h3><strong>Name:</strong> <br/>{ userState.name }</h3>
                </div>
                <div>
                    <h3><strong>Userame:</strong> <br/>{ userState.username }</h3>
                </div>
                <div>
                    <h3><strong>Email:</strong> <br/>{ userState.email }</h3>
                </div>
                <div>
                    <h3><strong>Phone:</strong> <br/>{ userState.phone }</h3>
                </div>
                <div>
                    <h3><strong>Website:</strong> <br/>{ userState.website }</h3>
                </div>
            </div>
            
            <div className="caja-detail">
                <div>
                    <h3><strong>Address:</strong> <br/>{ address.street } { address.suite }
                    </h3>
                </div>
                <div>
                    <h3><strong>City:</strong> <br/>{ address.city }</h3>
                </div>
                <div>
                    <h3><strong>Zipcode:</strong> <br/>{ address.zipcode }</h3>
                </div>
            </div>
            <div>
                <h1>User Company</h1>
            </div>
            <div className="caja-detail">
                <div>
                    <h3><strong>Name</strong> <br/> { company.name }
                    </h3>
                </div>
                <div>
                    <h3><strong>Catch Phrase:</strong> <br/>{ company.catchPhrase }</h3>
                </div>
                <div>
                    <h3><strong>Bs:</strong> <br/> { company.bs }</h3>
                </div>
            </div>
            
            <div className="caja-detail2">
                <div>
                    <button type="button" className="btn-user-update" onClick={Update}>
                        Edit
                    </button> 
                </div>
                <div>
                    <button type="button" className="btn-user-delete" onClick={Delete}>
                        Delete
                    </button>
                </div>
            </div>

            <div>

                { updateButton && <div className="form-detail">
                    <UpdateForm ActualizarForm={ActualizarForm} id={userState.id}/>
                </div> }

            </div>
        </div>
    )
}

export default UserDetail
