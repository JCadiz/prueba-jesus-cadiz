import Intro from '../components/Home/Intro'
import Card from '../components/Home/Card'
import Movil from '../components/Home/Movil'
import Chose from '../components/Home/ChoseCard'

const Home = () => {
    return (
        <div>
            <Intro/>
            <Card/>
            <Movil/>
            <Chose/>
        </div>
    )
}

export default Home
