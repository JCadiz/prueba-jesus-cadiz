import { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { setUsers } from '../services/actions/usersActions'
import User from '../components/Users/User'
import '../css/Users/Users.css'
import axios from 'axios'

const Users = () => {
    const userState = useSelector((state) => state.allUsers.users);
    const userStateEliminado = useSelector((state) => state.allUserAfterDelete);
    const dispatch = useDispatch();

    const fetchUserLists = async () => {
        const res = await axios.get("https://jsonplaceholder.typicode.com/users").catch(err => {
            console.log(err)
            });

        dispatch(setUsers(res.data));
    }

    useEffect(() => {
        
        fetchUserLists();
        
    }, []);

    return (
        <div className="bloque-users">
            {userState.map((user) => (
                <User key={user.id} user={user}/>
            ))}
        </div>
    )
}

export default Users
