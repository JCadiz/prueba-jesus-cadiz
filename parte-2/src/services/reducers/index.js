import { combineReducers } from 'redux'
import { userReducer, selectedUserReducer, deleteUser } from './userReducer'

const reducers = combineReducers({
    allUsers: userReducer,
    user: selectedUserReducer,
    allUserAfterDelete: deleteUser
});

export default reducers;