import { ActionTypes } from "../constants/action-types";
import axios from 'axios';

const initialState = {
    users: [],
};

export const userReducer = (state = initialState, {type, payload}) => {
    switch (type) {
        case ActionTypes.SET_USERS:
            return { ...state , users: payload };
        default:
            return state;
    }
};

export const selectedUserReducer = (state = [], {type, payload}) => {
    switch (type) {
        case ActionTypes.SELECTED_USER:
            return { ...state , ...payload };
        default:
            return state;
    }
};

export const deleteUser = async (state = [], {type, payload}) => {
    switch (type) {
        case ActionTypes.DELETE_USER:
            const res = await axios.get("https://jsonplaceholder.typicode.com/users").catch(err => {
                console.log(err)
            });

            let aux = res.data;

            let obj = aux.filter((elementoBorrar) => {
                if(elementoBorrar.id !== parseInt(payload)){
                    return elementoBorrar;
                }
            })

            state = obj.rol;

            return state;
        default:
            return state;
    }
};