import { NavLink } from 'react-router-dom'
import { BsArrowRightShort } from "react-icons/bs"
import logo from '../assets/visa-logo.png'
import '../css/Nav.css'

const Nav = () => {
    return (
        <nav className="fondo">
            <div className="caja">
                <div className="">
                    <img src={logo} alt="logo visa" className="logo"></img>
                </div>
                <div>
                    <ul>
                        <li className="">
                            <NavLink exact to="/" className="Link" activeClassName="active">Home</NavLink>
                        </li>
                        <li className="">
                            <NavLink exact to="/users" className="Link" activeClassName="active">Users</NavLink>
                        </li>
                        <li className="">Developing</li>
                        <li className="">Company</li>
                    </ul>
                </div>
                <div className="">
                    <ul>
                        <li>Support</li>
                        <div className="div-icono">
                            <li>Sign in</li><BsArrowRightShort className="icono"/>
                        </div>
                    </ul>
                </div>
            </div>
        </nav>
    )
}

export default Nav
