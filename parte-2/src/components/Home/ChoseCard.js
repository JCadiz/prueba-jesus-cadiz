import '../../css/Home/ChoseCard.css'
import tarjetaMorada from '../../assets/Home/Tarjeta-Visa.png'
import tarjetaVerde from '../../assets/Home/Tarjeta-Visa-verde.png'
import tarjetaRoja from '../../assets/Home/Tarjeta-Visa-rosa.png'

const ChoseCard = () => {
    return (
        <div className="bloque-chose">
            <div className="caja-chose margin-chose">
                <h1 className="chose-h1">Chose Your Card</h1>
                <p className="chose-p">is a great way to send money to friends and family even if they bank somewhere different than you do.
                    <br/> even if they bank somewhere different than you do
                </p>
            </div>
            <div className="caja-chose2 margin-chose">
                <img src={tarjetaMorada} alt="tarjeta morada" className="img-tarjetasChose"></img>
                <img src={tarjetaVerde} alt="tarjeta verde" className="img-tarjetasChose"></img>
                <img src={tarjetaRoja} alt="tarjeta roja" className="img-tarjetasChose"></img>
            </div>
        </div>
    )
}

export default ChoseCard
