import '../../css/Home/Movil.css'
import punto from '../../assets/Home/punto.png'
import iphone from '../../assets/Home/iphone.png'

const App = () => {
    return (
        <div className="bloque-Movil">
            <div className="caja-movil">
                <h1 className="movil-h1">Purchase Anytime</h1>

                <p className="movil-p">is a great way to send money to friends and family, even if they bank somewhere different than you do.  
                That means it’s super easy to pitch in or get paid back for all sorts of things like the neighborhood </p>

                <button type="button" className="btn-movil">Apply for Card</button>
            </div>
            <div className="caja-movil2" >
                <img src={iphone} alt="iphone" className="img-iphone"></img>
                <img src={punto} alt="punto" className="img-punto"></img>  
                <h4 className="h4-img">Shiping card..</h4>
            </div>
        </div>
    )
}

export default App
