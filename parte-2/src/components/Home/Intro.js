import tarjeta from '../../assets/Home/Tarjeta-Visa.png'
import fondo from '../../assets/Home/fondo-ovalado.png'
import '../../css/Home/Intro.css'

const Intro = () => {
    return (
        <div className="bloque-Intro" style={{ 
            backgroundImage: 'url('+fondo+')',
            backgroundSize: 'cover',
            backgroundPosition: 'center'
        }}>
            <div className="caja-Intro Intro-margin">
                <h1 className="Intro-h1">Be Smart Take Card</h1>
            </div>
            <div className="caja-Intro">
                <h5 className="Intro-h3">So you have your new digital money and wallet away to going anywhere and <br/>
                everything in sight. Now you want to print them and you need.
                </h5>
            </div>
            <div className="caja-Intro Intro-margin-btn">
                <button type="button" className="btn">Apply for  Card</button>
            </div>
            <div className="caja-Intro">
                <img src={tarjeta} alt="tarjeta intro" className="card-img"></img>
            </div>
        </div>
    )
}

export default Intro
