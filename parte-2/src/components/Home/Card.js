import '../../css/Home/Card.css'
import { BsPencilSquare, BsLayoutTextSidebarReverse,  BsImages} from "react-icons/bs"

const Card = () => {
    return (
        <div className="bloque-Card">
            <div className="caja-Card card-margin">
                <h1 className="Card-h1">Get Your Card</h1>
                <p className="Card-p">Financial advice to help face a new reality, Goals are not met alone. Plan them<br/>
                    An easy way to set and view your short-term and long-term financial goals
                </p>
            </div>
            <div className="caja-Card2">
                <div className="caja-tarjeta">
                    <div className="icono-cajita">
                        <BsPencilSquare className="icono-card"/>
                    </div>
                    <div>
                        <p className="Card-p">is a great way to send money to friends and family, even if they bank somewhere different than you do.  
                        That means it’s super easy to pitch in or get paid back for all sorts of things like the neighborhood </p>
                    </div>
                </div>
                <div className="caja-tarjeta shadow">
                    <div className="icono-cajita2">
                        <BsLayoutTextSidebarReverse className="icono-card2"/>
                    </div>
                    <div>
                        <p className="Card-p">is a great way to send money to friends and family, even if they bank somewhere different than you do.  
                        That means it’s super easy to pitch in or get paid back for all sorts of things like the neighborhood </p>
                    </div>
                </div>
                <div className="caja-tarjeta">
                    <div className="icono-cajita">
                        <BsImages className="icono-card"/>
                    </div>
                    <div>
                        <p className="Card-p">is a great way to send money to friends and family, even if they bank somewhere different than you do.  
                        That means it’s super easy to pitch in or get paid back for all sorts of things like the neighborhood </p>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Card
