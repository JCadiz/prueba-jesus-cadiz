import { NavLink } from 'react-router-dom'
import '../../css/Users/Users.css'

const User = ({user}) => {
    return (
        <div className="bloque-user">
            <div>
                <h3>{ user.name }</h3>
            </div>
            <div>
                <h3>{ user.username }</h3>
            </div>
            <div>
                <h3>{ user.email }</h3>
            </div>
            <section>
                <button type="button" className="btn-user">
                    <NavLink exact to={'/users/'+user.id} className="link"> More Details</NavLink>
                </button>
            </section>
        </div>
    )
}

export default User
