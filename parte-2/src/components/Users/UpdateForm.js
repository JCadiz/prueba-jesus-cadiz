import { useState } from 'react'
import '../../css/Users/UpdateForm.css'

const UpdateForm = ({ ActualizarForm, id }) => {
    const [name, setName] = useState('')
    const [username, setUsername] = useState('')
    const [email, setEmail] = useState('')
    const [phone, setPhone] = useState('')
    const [website, setWebsite] = useState('')
    const [street, setStreet] = useState('')
    const [suite, setSuite] = useState('')
    const [city, setCity] = useState('')
    const [zipcode, setZipcode] = useState('')
    const [latitude, setLatitude] = useState('')
    const [longitude, setLongitude] = useState('')
    const [Cname, setCname] = useState('')
    const [catchof, setCatch] = useState('')
    const [bs, setBs] = useState('')

    const onSubmit = (e) =>{
        e.preventDefault()

        if (!name || !username || !email || !phone || !website || !street || 
            !suite || !city || !zipcode || !latitude || !longitude || !Cname || !catchof || !bs){
                alert('Please fill the form completely');
                return;
            }

        ActualizarForm({ 
            "id": id, 
            "name": name,
            "username":username,
            "email": email, 
            "address":{
                "street":street,
                "suite": suite,
                "city": city,
                "zipcode": zipcode,
                "geo":{
                    "lat": latitude,
                    "lng": longitude
                }
            },
            "phone": phone, 
            "website": website, 
            "company": {
                "name": Cname,
                "catchPhrase":  catchof,
                "bs": bs
            }
           })

        setName('');
        setUsername('');
        setEmail('');
        setPhone('');
        setWebsite('');
        setStreet('');
        setSuite('');
        setCity('');
        setZipcode('');
        setLatitude('');
        setLongitude('');
        setCname('');
        setCatch('')
        setBs('');
    }

    return (
        <form onSubmit={onSubmit}>
            <div>
                <h1>Update Form</h1>
            </div>
            <div className="form-group">
                <div>
                    <label>Name</label>
                    <input type="text" value={name} onChange={(e) => setName(e.target.value)} placeholder="Change Name" required/>
                </div>
                <div>
                    <label>Username</label>
                    <input type="text" value={username} onChange={(e) => setUsername(e.target.value)} placeholder="Change Username" required/>
                </div>
                <div>
                    <label>Email</label>
                    <input type="email" value={email} onChange={(e) => setEmail(e.target.value)} placeholder="Change Email" required/>
                </div>
                <div>
                    <label>Phone</label>
                    <input type="text" value={phone} onChange={(e) => setPhone(e.target.value)} placeholder="Change Phone" required/>
                </div>
                <div>
                    <label>Website</label>
                    <input type="text" value={website} onChange={(e) => setWebsite(e.target.value)} placeholder="Change Website" required/>
                </div>
            </div>
            <div className="form-group">
                <div>
                    <label>Street</label>
                    <input type="text" value={street} onChange={(e) => setStreet(e.target.value)} placeholder="Change Street" required/>
                </div>
                <div>
                    <label>Suite</label>
                    <input type="text" value={suite} onChange={(e) => setSuite(e.target.value)} placeholder="Change Suite" required/>
                </div>
                <div>
                    <label>City</label>
                    <input type="text" value={city} onChange={(e) => setCity(e.target.value)} placeholder="Change City" required/>
                </div>
                <div>
                    <label>Zipcode</label>
                    <input type="text" value={zipcode} onChange={(e) => setZipcode(e.target.value)} placeholder="Change Zipcode" required/>
                </div>
                <div>
                    <label>Latitude</label>
                    <input type="text" value={latitude} onChange={(e) => setLatitude(e.target.value)} placeholder="Change Latitude" required/>
                </div>
                <div>
                    <label>Longitude</label>
                    <input type="text" value={longitude} onChange={(e) => setLongitude(e.target.value)} placeholder="Change Longitude" required/>
                </div>
            </div>
            <div className="form-group">
                <div>
                    <label>Company Name</label>
                    <input type="text" value={Cname} onChange={(e) => setCname(e.target.value)} placeholder="Change company name" required/>
                </div>
                <div>
                    <label>Catch Phrase</label>
                    <input type="text" value={catchof} onChange={(e) => setCatch(e.target.value)} placeholder="Change catch Phrase" required/>
                </div>
                <div>
                    <label>Bs</label>
                    <input type="text" value={bs} onChange={(e) => setBs(e.target.value)} placeholder="Change Bs" required/> 
                </div>
            </div>
            <div className="submit-caja">
                <input type="submit" value="update" className=""/>
            </div>
        </form>
    )
}

export default UpdateForm
