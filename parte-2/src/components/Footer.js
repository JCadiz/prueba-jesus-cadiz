import fondo from '../assets/fondo-gris.png'
import { FaGooglePlusG, FaFacebookF, FaTwitter, FaLinkedin, FaCartPlus } from "react-icons/fa";
import '../css/Footer.css'

const Footer = () => {
    return (
        <footer style={{ 
            backgroundImage: 'url('+fondo+')',
            backgroundSize: 'cover',
            backgroundPosition: 'center',
            height: 'auto',
        }}>

            <div className="bloque-footer margin-footer footer-bg" >
                <div className="caja-footer margin2-footer">
                    <div className="sub-caja-footer">
                        <ul>
                            <li className="footer-color">PRODUCTS</li>
                            <li >About us</li>
                            <li >Contact</li>
                            <li >Pricing Tax</li>
                        </ul>
                    </div>
                    <div className="sub-caja-footer">
                        <ul>
                            <li className="footer-color">DEVELOPERS</li>
                            <li >Work with us</li>
                            <li >FAQ</li>
                            <li >Address</li>
                            <li >Value info</li>
                        </ul>
                    </div>
                    <div className="sub-caja-footer">
                        <ul>
                            <li className="footer-color">COMPANY</li>
                            <li >Train Guest</li>
                            <li >Invest</li>
                            <li >Data</li>
                        </ul>
                    </div>
                    <div className="sub-caja-footer2">
                        <h5 className="footer-color">NEWSLETTER</h5>
                        <div className="footer-input">
                            <input type="text" placeholder="Email address"></input>
                            <div className="bg-footer-icon">
                                <FaCartPlus className="icono2-footer"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="caja-footer">
                    <div className="caja-redes">
                        <ul>
                            <li className="footer-color">ICMP</li>
                            <li >SERVICE</li>
                            <li >ABOUT</li>
                            <li >SUPPORT</li>
                        </ul>
                    </div>
                    <div className="caja-redes2">
                        <div className="div-icono-footer">
                            <FaFacebookF className="icono-footer"/>
                        </div>
                        <div className="div-icono-footer">
                            <FaTwitter className="icono-footer"/>
                        </div>
                            <div className="div-icono-footer">
                            <FaLinkedin className="icono-footer"/>
                        </div>
                        <div className="div-icono-footer">
                            <FaGooglePlusG className="icono-footer"/>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    )
}

export default Footer
