import { BrowserRouter as Router, Route, Switch } from "react-router-dom"
import Home from "./pages/Home"
import Users from "./pages/Users"
import UserDetail from "./pages/UserDetail"
import Nav from './components/Nav'
import Footer from './components/Footer'

export default function AppRouter() {
    return (
        <div>
            <Router>
                <Nav />
                <Switch>
                    <Route exact path="/users" component={Users}/>
                    <Route exact path="/" component={Home}/>
                    <Route exact path="/users/:ID" component={UserDetail}/>
                </Switch>
                <Footer/>
            </Router> 
        </div>
    );
}
