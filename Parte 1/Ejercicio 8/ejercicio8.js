/*
Write a function called vowelCount which accepts a string and returns an object with the keys as 
the vowel and the values as the number of times the vowel appears in the string. 
This function should be case insensitive so a lowercase letter and uppercase letter should count
Examples:
    vowelCount('Elie') // {e:2,i:1};
    vowelCount('Tim') // {i:1};
    vowelCount('Matt') // {a:1})
    vowelCount('hmmm') // {};
    vowelCount('I Am awesome and so are you') // {i: 1, a: 4, e: 3, o: 3, u: 1};
*/

const v1 = 'Elie';
const v2 = 'Tim';
const v3 = 'Matt';
const v4 = 'hmmm';
const v5 = 'I Am awesome and so are you';

function vowelCount(str){
    let vocaleSinMayusculas = str.toLowerCase();
    let vocales = vocaleSinMayusculas.replace(/[bcdfghjklmnpqrstvxyz]/ig,'');

    //contar cuantas a, e, i, o, u
    let countA = (vocales.match(/a/g)|| []).length;
    let countE = (vocales.match(/e/g)|| []).length;
    let countI = (vocales.match(/i/g)|| []).length;
    let countO = (vocales.match(/o/g)|| []).length;
    let countU = (vocales.match(/u/g)|| []).length;

    
    let objetoVocal = {}

    if (countA != 0){
        objetoVocal.a = countA;
    }if (countE != 0){
        objetoVocal.e = countE;
    }if (countI != 0){
        objetoVocal.i = countI;
    }if (countO != 0){
        objetoVocal.o = countO;
    }if (countU != 0){
        objetoVocal.u = countU;
    }
    
    return objetoVocal;
}

console.log(vowelCount(v1));
console.log(vowelCount(v2));
console.log(vowelCount(v3));
console.log(vowelCount(v4));
console.log(vowelCount(v5));