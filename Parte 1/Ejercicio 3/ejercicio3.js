/*
Write a function called findInObj which accepts an array of objects, a key, and some value to search for 
and returns the first found value in the array.
Examples:
    findInObj([{first: 'Elie', last:"Schoppik"}, {first: 'Tim', last:"Garcia", isCatOwner: true}, 
    {first: 'Matt', last:"Lane"}, {first: 'Colt', last:"Steele", isCatOwner: true}], 'isCatOwner',true) 
    // {first: 'Tim', last:"Garcia", isCatOwner: true}
*/

const arr = [{first: 'Elie', last:"Schoppik"}, {first: 'Tim', last:"Garcia", isCatOwner: true}, 
    {first: 'Matt', last:"Lane"}, {first: 'Colt', last:"Steele", isCatOwner: true}];

function findInObj(arr, key, searchValue){
    
    let filtro = arr.filter((verificar)=>{
        if(key in verificar){
            return verificar;
        }
    });

    const resultadoObj = filtro.find(res => {
        if(res.isCatOwner == searchValue){
            return res;
        }else if (res.first == searchValue){
            return res;
        }else if (res.last == searchValue){
            return res;
        }
    });
  
    return resultadoObj;

}

let result = findInObj(arr, 'isCatOwner', true);

console.log(result);