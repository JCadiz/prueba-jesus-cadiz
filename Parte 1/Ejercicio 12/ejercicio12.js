/*
Write a function called hasCertainKey which accepts an array of objects and a key, and returns true if every single 
object in the array contains that key. Otherwise it should return false.
Examples:
    var arr = [
        {title: "Instructor", first: 'Elie', last:"Schoppik"}, 
        {title: "Instructor", first: 'Tim', last:"Garcia", isCatOwner: true}, 
        {title: "Instructor", first: 'Matt', last:"Lane"}, 
        {title: "Instructor", first: 'Colt', last:"Steele", isCatOwner: true}
    ]
    
    hasCertainKey(arr,'first') // true
    hasCertainKey(arr,'isCatOwner') // false
*/

let arr = [
        {title: "Instructor", first: 'Elie', last:"Schoppik"}, 
        {title: "Instructor", first: 'Tim', last:"Garcia", isCatOwner: true}, 
        {title: "Instructor", first: 'Matt', last:"Lane"}, 
        {title: "Instructor", first: 'Colt', last:"Steele", isCatOwner: true}
    ]

function hasCertainKey(arr, key){
    let verificarVerdadero = 0;

    for (const index in arr){
        if(arr[index].hasOwnProperty(key)){
            verificarVerdadero += 1;
        }
    }

    return verificarVerdadero == 4? true: false;
}

console.log(hasCertainKey(arr,'first')); 
console.log(hasCertainKey(arr,'isCatOwner'));