/*
Write a function called hasNoDuplicates which accepts an array and returns true if there are no duplicate 
values (more than one element in the array that has the same value as another). If there are any duplicates,
the function should return false.
Examples:
    hasNoDuplicates([1,2,3,2]) // false
    hasNoDuplicates([1,2,3]) // true
*/

const arr1 = [1,2,3,2];
const arr2 = [1,2,3];

function hasNoDuplicates(arr){  
    return !arr.some((element, i) => {
       return arr.indexOf(element) !== i
    });
}

console.log(hasNoDuplicates(arr1));
console.log(hasNoDuplicates(arr2));