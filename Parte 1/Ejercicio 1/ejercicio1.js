/*
Write a function called filterByValue which accepts an array of objects and a
key and returns a new array with all the objects that contain that key.
Examples:
    filterByValue([{first: 'Elie', last:"Schoppik"}, {first: 'Tim', last:"Garcia", isCatOwner: true}, 
    {first: 'Matt', last:"Lane"}, {first: 'Colt', last:"Steele", isCatOwner: true}], 'isCatOwner') 
    // [{first: 'Tim', last:"Garcia", isCatOwner: true}, {first: 'Colt', last:"Steele", isCatOwner: true}]
*/

const arr = [{first: 'Elie', last:"Schoppik"}, {first: 'Tim', last:"Garcia", isCatOwner: true}, 
{first: 'Matt', last:"Lane"}, {first: 'Colt', last:"Steele", isCatOwner: true}];

const key = 'isCatOwner';

function filterByValue(arr, key){
    let filtro = [];
    
    filtro = arr.filter((verificar)=>{
        if(key in verificar){
            return verificar;
        }
    });
  
    return filtro;
}

let result = filterByValue(arr, key);

console.log(result);


