/*
Write a function called doubleOddNumbers which accepts an array and returns a new array with all of the odd 
numbers doubled (HINT - you can use map and fitler to double and then filter the odd numbers).
Examples:
    doubleOddNumbers([1,2,3,4,5]) // [2,6,10]
    doubleOddNumbers([4,4,4,4,4]) // []
*/
const a1 = [1,2,3,4,5];
const a2 = [4,4,4,4,4];

function doubleOddNumbers(arr){
    return arr.filter((val) => {
        return val%2 !==0;
    }).map((valor) => {
        return valor*2;
    })
}

console.log(doubleOddNumbers(a1));

console.log(doubleOddNumbers(a2));