/*
Write a function called extractKey which accepts an array of objects and some key and returns a new array 
with the value of that key in each object.
Examples:
    extractKey([{name: 'Elie'}, {name: 'Tim'}, {name: 'Matt'}, {name: 'Colt'}], 'name') 
    // ['Elie', 'Tim', 'Matt', 'Colt']
*/
const obj = [{name: 'Elie'}, {name: 'Tim'}, {name: 'Matt'}, {name: 'Colt'}];

function extractKey(arr, key){

    let filtro = arr.filter((verificar)=>{
        if(key in verificar){
            return verificar;
        }
    });
    
    let arreglo = [];

    Object.keys(filtro).forEach(val => {
       arreglo.push(filtro[val].name);
    });

    return arreglo;
}

console.log(extractKey(obj, 'name'));