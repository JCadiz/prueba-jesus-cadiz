/*
Write a function called removeVowels which accepts a string and returns a new string with all of the vowels 
(both uppercased and lowercased) removed. Every character in the new string should be lowercased.
Examples:
    removeVowels('Elie') // ('l')
    removeVowels('TIM') // ('tm')
    removeVowels('ZZZZZZ') // ('zzzzzz')
*/

const v1 = 'Elie';
const v2 = 'TIM';
const v3 = 'ZZZZZZ';

function removeVowels(str){
    let consonantes = str.toLowerCase();

    return consonantes.replace(/[aeiou]/ig,'');
}

console.log(removeVowels(v1));
console.log(removeVowels(v2));
console.log(removeVowels(v3));