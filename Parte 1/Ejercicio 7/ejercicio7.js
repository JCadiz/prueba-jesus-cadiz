/*
Write a function called extractValue which accepts an array of objects and a key and returns a new array 
with the value of each object at the key.
Examples:
    var arr = [{name: 'Elie'}, {name: 'Tim'}, {name: 'Matt'}, {name: 'Colt'}]
    extractValue(arr,'name') // ['Elie', 'Tim', 'Matt', 'Colt']
*/

//Nota este ejercicio es identico al ejercicio anterior

const arr = [{name: 'Elie'}, {name: 'Tim'}, {name: 'Matt'}, {name: 'Colt'}];

function extractValue(arr, key){

   let filtro = arr.filter((verificar)=>{
        if(key in verificar){
            return verificar;
        }
    });
    
    let arreglo = [];

    Object.keys(filtro).forEach(val => {
       arreglo.push(filtro[val].name);
    });

    return arreglo;
}

console.log(extractValue(arr, 'name'));